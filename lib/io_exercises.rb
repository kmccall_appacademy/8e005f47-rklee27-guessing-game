# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  correct_number = rand(1..100)
  guess = nil
  guess_counter = 0

  until correct_number == guess
    puts "Guess a number."
    guess = gets.chomp.to_i

    if guess > correct_number
      puts "#{guess} was too high. Guess again."
    elsif guess < correct_number
      puts "#{guess} was too low. Guess again."
    end

    guess_counter += 1
    puts "This was attempt ##{guess_counter}"
  end

  puts "Congradulations, you have guessed #{correct_number} correctly!"
  puts "You took #{guess_counter} attempt(s)"
end

def file_shuffler
  puts "What is the file name?"
  file_name = gets.chomp
  file_name_no_extension = file_name[0...-4]
  contents = File.readlines(file_name)
  shuffled = File.open("#{file_name_no_extension}-shuffled.txt", "w")
  shuffled.puts contents.shuffle
  shuffled.close
end

# if __FILE__ == $PROGRAM_NAME
#   file_shuffler
# end
